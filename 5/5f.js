
 class Person{
 constructor(firstName,lastName){
  this.firstName=firstName;
  this.lastName=lastName;
 }
    get FullName(){
    return this.firstName + " " + this.lastName;
    }
     setFullName(name,surname){
    this.firstName=name;
    this.lastName=surname;
  }
  
   
}
 class Student extends Person{
 constructor(firstName,lastName,id,grades){
  super(firstName,lastName);
  this.id=id;
  this.grades=grades;
 }
 get AVG() {
   return this.calcAVG();
 }
  calcAVG(){
    var sum=0;
    var average=0;  
    for (var num in this.grades) {
      sum=sum+this.grades[num];
      average=sum/this.grades.length;
    }
    return average;
     
  }
   
}


var student = new Student('Bob','Baratheon',4,[3,4,5,5]);
console.log(student.AVG);
student.setFullName('Zubenko','Michael');
console.log(student.FullName);