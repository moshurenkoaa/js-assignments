 function Student(firstName,lastName,id,grades){
  this.firstName=firstName;
  this.lastName=lastName;
  this.id=id;
  this.grades=grades;

  this.getAVG= function(){
    var sum=0;
    var average=0;  
    for (var num in grades) {
      sum=sum+grades[num];
      average=sum/grades.length;
    }
    return average;
     
  }
  this.getFullName = function(){
    return firstName + " " + lastName;
  }
  this.setFullName= function(name,surname){
    firstName=name;
    lastName=surname;
  }
}
var student = new Student('Bob','Baratheon',4,[3,4,5,5]);
console.log(student.getAVG());
console.log(student.getFullName());
student.setFullName('Zubenko','Michael');
console.log(student.getFullName());