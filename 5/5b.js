 function Student(firstName,lastName,id,grades){
  this.firstName=firstName;
  this.lastName=lastName;
  this.id=id;
  this.grades=grades;

  this.getInfo = function() {
    var sum=0;
    var average=0
    for (var num in grades) {
      sum=sum+grades[num];
      average=sum/(grades.length)
    
    }
    console.log('Students name is '+firstName);
    console.log('Students lastname is '+lastName);
    console.log('Students average grade is '+average);
  }
}
var student = new Student('Bob','Baratheon',4,[3,4,5,5]);
student.getInfo();